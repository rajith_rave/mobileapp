package com.mobilebanking;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class Constants {

    public static final String DEVICE_ID = "DeviceId";
    public static final String USER_ID = "UserId";
    public static final String PLATFORM = "Android";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PASSWORD = "Password";
    public static final String EMAIL = "Email";
    public static final String MOBILE_NUMBER = "MobileNumber";
    public static final String LOGIN_ID = "LoginId";
    public static final String SECURITY_TYPE_ID = "SecurityTypeId";
    public static final String SECURITY_STATUS = "SecurityStatus";
    public static final String E_BANKING_CHECKBOX = "ebanking";
    public static final String M_BANKING_CHECKBOX = "mbanking";
    public static final String ATM_CHECKBOX = "atmbanking";
    public static final String SELECTED_SECURITY_TYPE = "selected_security";
    public static final String SIGN_IN_STATUS = "SignInStatus";
    public static final String SUCCESS = "Success";
    public static final String LOCATION = "location";
}
