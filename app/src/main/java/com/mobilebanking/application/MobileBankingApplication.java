package com.mobilebanking.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.mobilebanking.services.ConnectivityReceiver;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.log.CustomLogger;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class MobileBankingApplication extends MultiDexApplication {

    private static final String TAG = "MobileBankingApplication";

    private static MobileBankingApplication mobileBankingApplication;
    private JobManager jobManager;


    public static MobileBankingApplication getInstance() {
        return mobileBankingApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mobileBankingApplication = this;
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        configureJobManager();
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }

    private void configureJobManager() {

        Configuration configuration = new Configuration.Builder(this)
                .customLogger(new CustomLogger() {


                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                }).minConsumerCount(0)// always keep at least five consumer
                // alive
                .maxConsumerCount(10)// up to 10 consumers at a time
                .loadFactor(3)// 3 jobs per consumer
                .consumerKeepAlive(120)// wait 2 minute
                .build();
        jobManager = new JobManager(this, configuration);
    }



    public JobManager getJobManager() {
        return jobManager;
    }



    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


}
