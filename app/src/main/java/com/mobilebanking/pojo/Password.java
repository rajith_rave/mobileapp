package com.mobilebanking.pojo;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class Password {


    private final String login_id;
    private final String product_code;
    private final String password;
    private final String device_id;
    private final String push_token;

    public Password(String login_id, String product_code,String password,String device_id,String push_token) {
        this.login_id = login_id;
        this.product_code = product_code;
        this.push_token = push_token;
        this.device_id = device_id;
        this.password = password;

    }


}