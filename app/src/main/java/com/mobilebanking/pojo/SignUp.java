package com.mobilebanking.pojo;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class SignUp {

    private final String first_name;
    private final String last_name;
    private final String login_id;
    private final String email_id;
    private final String mobile_no;
    private final String device_id;
    private final String push_token;
    private final String password;
    private final String device_type;

    public SignUp(String first_name, String last_name,String login_id,String email_id,String mobile_no,String device_id,String push_token,String password,String device_type) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.login_id = login_id;
        this.email_id = email_id;
        this.mobile_no = mobile_no;
        this.device_id = device_id;
        this.push_token = push_token;
        this.password = password;
        this.device_type = device_type;

        System.out.println("SignUpJob CHECKING first_name" + first_name);
        System.out.println("SignUpJob CHECKING last_name" + last_name);
        System.out.println("SignUpJob CHECKING login_id" + login_id);
        System.out.println("SignUpJob CHECKING email_id" + email_id);
        System.out.println("SignUpJob CHECKING mobile_no" + mobile_no);
        System.out.println("SignUpJob CHECKING device_id" + device_id);
        System.out.println("SignUpJob CHECKING push_token" + push_token);
        System.out.println("SignUpJob CHECKING password" + password);
        System.out.println("SignUpJob CHECKING device_type" + device_type);
    }


}