package com.mobilebanking.pojo;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class Login {


    private final String login_id;
    private final String product_code;
    private final String latitude;
    private final String device_id;
    private final String longitude;

    public Login(String login_id, String product_code,String latitude,String device_id,String longitude) {
        this.login_id = login_id;
        this.product_code = product_code;
        this.latitude = latitude;
        this.device_id = device_id;
        this.longitude = longitude;

    }


}