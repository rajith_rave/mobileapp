package com.mobilebanking.events;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class SetSecurityTypesJobCompletedEvent {

    private String result;

    public SetSecurityTypesJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
