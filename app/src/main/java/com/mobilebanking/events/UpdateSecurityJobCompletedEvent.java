package com.mobilebanking.events;

/**
 * Created by Tharindu on 5/2/2017.
 */

public class UpdateSecurityJobCompletedEvent {

    private String result;

    public UpdateSecurityJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
