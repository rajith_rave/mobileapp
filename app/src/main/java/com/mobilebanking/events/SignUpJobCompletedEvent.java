package com.mobilebanking.events;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class SignUpJobCompletedEvent {

    private String result;

    public SignUpJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
