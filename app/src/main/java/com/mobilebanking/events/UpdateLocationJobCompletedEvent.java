package com.mobilebanking.events;

/**
 * Created by Tharindu on 5/2/2017.
 */

public class UpdateLocationJobCompletedEvent {

    private String result;

    public UpdateLocationJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
