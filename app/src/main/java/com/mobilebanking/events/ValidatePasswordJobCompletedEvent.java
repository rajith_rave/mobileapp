package com.mobilebanking.events;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class ValidatePasswordJobCompletedEvent {

    private String result;

    public ValidatePasswordJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
