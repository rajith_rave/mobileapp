package com.mobilebanking.events;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class VerifySMSJobCompletedEvent {

    private String result;

    public VerifySMSJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}

