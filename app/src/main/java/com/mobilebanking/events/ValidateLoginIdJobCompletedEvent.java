package com.mobilebanking.events;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class ValidateLoginIdJobCompletedEvent {

    private String result;

    public ValidateLoginIdJobCompletedEvent(String result) {
        this.result = result;

    }

    public String getResult() {
        return result;
    }
}
