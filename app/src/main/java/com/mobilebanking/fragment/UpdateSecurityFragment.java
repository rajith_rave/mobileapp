package com.mobilebanking.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.UpdateLocationJobCompletedEvent;
import com.mobilebanking.events.UpdateSecurityJobCompletedEvent;
import com.mobilebanking.jobs.UpdateLocationJob;
import com.mobilebanking.jobs.UpdateSecurityJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.ui.BiometricSecurityActivity;
import com.mobilebanking.ui.FingeprintActivity;
import com.mobilebanking.ui.LoginActivity;
import com.mobilebanking.ui.ProductsListActivity;
import com.mobilebanking.ui.SignUpActivity;
import com.mobilebanking.ui.UpdateSecurityActivity;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Tharindu on 5/2/2017.
 */

public class UpdateSecurityFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{

    private CheckBox eBankingCheckBox;
    private CheckBox mBankingCheckBox;
    private CheckBox atmCheckBox;
    private Button nextButton;
    private ProgressDialog progress;
    private JobManager jobManager;
    AlertDialog alertDialog;
    public static Fragment newInstance(Context context) {
        UpdateSecurityFragment f = new UpdateSecurityFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_update_security, null);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        eBankingCheckBox  = (CheckBox) root.findViewById(R.id.e_banking_checkbox);
        mBankingCheckBox  = (CheckBox)  root.findViewById(R.id.m_banking_checkbox);
        atmCheckBox  = (CheckBox)  root.findViewById(R.id.atm_checkbox);
        nextButton  = (Button)  root.findViewById(R.id.next_button);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
        final SharedPreferences.Editor editor = preferences.edit();



        boolean iseBankingSelected = preferences.getBoolean(Constants.E_BANKING_CHECKBOX,false);
        System.out.println("checking final json 555555555 " + iseBankingSelected);

        if(iseBankingSelected){
            eBankingCheckBox.setChecked(true);

        }else{
            eBankingCheckBox.setChecked(false);

        }
        boolean ismBankingSelected = preferences.getBoolean(Constants.M_BANKING_CHECKBOX,false);
        System.out.println("checking final json 666666666666 " + ismBankingSelected);

        if(ismBankingSelected){
            mBankingCheckBox.setChecked(true);


        }else{
            mBankingCheckBox.setChecked(false);

        }
        boolean isatmBankingSelected = preferences.getBoolean(Constants.ATM_CHECKBOX,false);
        System.out.println("checking final json 77777777777 " + isatmBankingSelected);
        if(isatmBankingSelected){
            atmCheckBox.setChecked(true);

        }else{
            atmCheckBox.setChecked(false);

        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(eBankingCheckBox.isChecked() || mBankingCheckBox.isChecked() || atmCheckBox.isChecked()){
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    if(eBankingCheckBox.isChecked()){
                        editor.putBoolean(Constants.E_BANKING_CHECKBOX, true);

                        editor.commit();
                    }else{
                        editor.putBoolean(Constants.E_BANKING_CHECKBOX, false);

                        editor.commit();
                    }

                    if(mBankingCheckBox.isChecked()){
                        editor.putBoolean(Constants.M_BANKING_CHECKBOX, true);

                        editor.commit();
                    }else{
                        editor.putBoolean(Constants.M_BANKING_CHECKBOX, false);

                        editor.commit();
                    }
                    if(atmCheckBox.isChecked()){
                        editor.putBoolean(Constants.ATM_CHECKBOX, true);

                        editor.commit();
                    }else{
                        editor.putBoolean(Constants.ATM_CHECKBOX, false);

                        editor.commit();
                    }
                    if(eBankingCheckBox.isChecked()&&mBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){

                        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication, m-Banking Mobile App and ATM Account?");
                    }else if (eBankingCheckBox.isChecked()&&mBankingCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication and m-Banking Mobile App?");

                    }else if (eBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication and ATM Account?");

                    }else if (mBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the m-Banking Mobile App and ATM Account?");

                    }else if (mBankingCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the m-Banking Mobile App?");

                    }else if (atmCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the ATM Account?");

                    }else if (eBankingCheckBox.isChecked()){
                        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication?");

                    }

                }else{
                    Intent i = new Intent(getActivity(), UpdateSecurityActivity.class);
                    startActivity(i);
                }





            }
        });



        return root;
    }



    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(getActivity());
        final View dialogView = myLayout.inflate(R.layout.conformation_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView noTextView = (TextView) alertDialog.findViewById(R.id.no_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                Intent i = new Intent(getActivity(), UpdateSecurityActivity.class);
                startActivity(i);

            }
        });
        noTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }
}
