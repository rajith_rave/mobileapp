package com.mobilebanking.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SetSecurityTypesJobCompletedEvent;
import com.mobilebanking.events.UpdateLocationJobCompletedEvent;
import com.mobilebanking.jobs.SetSecurityTypesJob;
import com.mobilebanking.jobs.UpdateLocationJob;
import com.mobilebanking.ui.LoginActivity;
import com.mobilebanking.ui.SelectLocationActivity;
import com.mobilebanking.util;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;
import com.path.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import de.greenrobot.event.EventBus;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by Tharindu on 5/2/2017.
 */

public class UpdateLocationFragment extends Fragment {

    private EditText locationEditText;
    private Button submitButton;
    private CountryPicker mCountryPicker;
    private String country = "All Countries";
    private ProgressDialog progress;
    private JobManager jobManager;
    AlertDialog alertDialog;

    public static Fragment newInstance(Context context) {
        UpdateLocationFragment f = new UpdateLocationFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_update_location, null);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }

        locationEditText  = (EditText) root.findViewById(R.id.county_edittext);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
        final SharedPreferences.Editor editor = preferences.edit();
        country = preferences.getString(Constants.LOCATION, null);
        locationEditText.setText(country);
        submitButton  = (Button) root.findViewById(R.id.submit_button);
        mCountryPicker = CountryPicker.newInstance("Select Country");
        setListener();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("checking final json 11111111111111 ");
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                final SharedPreferences.Editor editor = preferences.edit();

                String deviceId = preferences.getString(Constants.DEVICE_ID, null);
                country = String.valueOf(locationEditText.getText());
                try {
                    System.out.println("checking final json 22222222 ");

                    JSONObject jsonObjSend = new JSONObject();
                    jsonObjSend.put("device_id", deviceId);
                    jsonObjSend.put("location", country);

                    System.out.println("checking final json 888888888 " + jsonObjSend.toString());
                    progress = new ProgressDialog(getActivity());
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setMessage("Loading");
                    progress.show();

                    updateLocationJob(jsonObjSend.toString());



                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("checking final json 99999999999 " + e);

                }




            }
        });
        return root;
    }

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                locationEditText.setText(name);
                country=name;
                mCountryPicker.dismiss();
            }
        });
        locationEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

    }
    private void updateLocationJob(String json) {


        jobManager.addJobInBackground(new UpdateLocationJob(json));
    }

    public void onEventBackgroundThread(final UpdateLocationJobCompletedEvent event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    System.out.println("UpdateLocationJobCompletedEvent 1111111111 ");

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        System.out.println("UpdateLocationJobCompletedEvent 2222222222 ");


                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {
                            System.out.println("UpdateLocationJobCompletedEvent 333333333333333 ");

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getActivity(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {
                            System.out.println("UpdateLocationJobCompletedEvent 44444444444444444 ");


                            System.out.println("UpdateLocationJobCompletedEvent 555555555555 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            editor.putString(Constants.LOCATION, country);
                            editor.commit();
                            showAlertDialog(statusMessage);
                            //                            Intent i = new Intent(SelectLocationActivity.this, SmsVerificationActivity.class);
//                            startActivity(i);

                        } else {

                            System.out.println("UpdateLocationJobCompletedEvent 66666666666666 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            util.errorMessage(getActivity());
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                        System.out.println("UpdateLocationJobCompletedEvent 777777777 " + e);

                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(getActivity());
                    }

                } else {
                    System.out.println("UpdateLocationJobCompletedEvent 88888888888 ");

                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(getActivity());

                }

            }
        });

    }

    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(getActivity());
        final View dialogView = myLayout.inflate(R.layout.submit_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finishAffinity();
            }
        });
    }
}
