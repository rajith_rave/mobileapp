package com.mobilebanking.jobs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.mobilebanking.Constants;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SignUpJobCompletedEvent;
import com.mobilebanking.pojo.SignUp;
import com.mobilebanking.util;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.squareup.okhttp.internal.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class SignUpJob extends Job {
    private static final String TAG = "SignUpJob";
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private static final long serialVersionUID = 1L;
    private final int id;


    private final String firstName;
    private final String lastName;
    private final String email;
    private final String password;
    private final String loginId;
    private final String mobiloenumber;
    private final String deviceId;


    private static final int PRIORITY = 1;


    public SignUpJob(String loginId, String firstName, String lastName, String email,String password, String deviceId, String mobiloenumber) {
        super(new Params(PRIORITY).persist().requireNetwork());

        id = jobCounter.incrementAndGet();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.loginId = loginId;
        this.mobiloenumber = mobiloenumber;
        this.deviceId = deviceId;

        System.out.println("SignUpJob CHECKING firstName" + firstName);
        System.out.println("SignUpJob CHECKING lastName" + lastName);
        System.out.println("SignUpJob CHECKING email" + email);
        System.out.println("SignUpJob CHECKING deviceId" + deviceId);
        System.out.println("SignUpJob CHECKING loginId" + loginId);
        System.out.println("SignUpJob CHECKING mobiloenumber" + mobiloenumber);
        System.out.println("SignUpJob CHECKING password" + password);


    }


    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {

        Log.d(TAG, "onRun start");

        if (id != jobCounter.get()) {


            Log.d(TAG, "onRun id != jobCounter.get()");

            return;
        }


        try {

            System.out.println("SignUpJob CHECKING try");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
            final SharedPreferences.Editor editor = preferences.edit();

            HttpEntity resEntity;

            HttpClient client = new DefaultHttpClient();



            String md5Password = util.MD5(password);
            String platform = Constants.PLATFORM;
            SignUp obj = new SignUp(firstName, lastName, loginId, email, mobiloenumber, deviceId, "12345",md5Password,platform);
            Gson gson = new Gson();
            final String json = gson.toJson(obj);
            System.out.println("SignUpJob CHECKING json " + json);
            String url = "http://209.10.93.234:8080/babylon/ws/signup";
            HttpPost post = new HttpPost(url);
            StringEntity postingString = null;//gson.tojson() converts your pojo to json

            postingString = new StringEntity(json);
            post.setEntity(postingString);

            post.setHeader("Content-Type", "application/json");


            String response_str = null;

            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            System.out.println("SignUpJob CHECKING response_str " + response_str);


            if (response_str != null) {
                EventBus.getDefault().post(new SignUpJobCompletedEvent(response_str));
            } else {
                EventBus.getDefault().post(new SignUpJobCompletedEvent(null));
            }



        } catch (Exception e) {
            System.out.println("SignUpJob CHECKING Exception " + e);

            EventBus.getDefault().post(new SignUpJobCompletedEvent(null));
        }

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


}
