package com.mobilebanking.jobs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.mobilebanking.Constants;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.ValidatePasswordJobCompletedEvent;
import com.mobilebanking.events.VerifySMSJobCompletedEvent;
import com.mobilebanking.pojo.Password;
import com.mobilebanking.pojo.VerifySMS;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class VerifySMSJob extends Job {
    private static final String TAG = "VerifySMSJob";
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private static final long serialVersionUID = 1L;
    private final int id;



    private final String smsCode;


    private static final int PRIORITY = 1;


    public VerifySMSJob(String smsCode) {
        super(new Params(PRIORITY).persist().requireNetwork());

        id = jobCounter.incrementAndGet();
        this.smsCode = smsCode;

        System.out.println("VerifySMSJob CHECKING firstName" + smsCode);

    }


    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {

        Log.d(TAG, "onRun start");

        if (id != jobCounter.get()) {


            Log.d(TAG, "onRun id != jobCounter.get()");

            return;
        }


        try {

            System.out.println("VerifySMSJob CHECKING try");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
            final SharedPreferences.Editor editor = preferences.edit();
            String deviceId = preferences.getString(Constants.DEVICE_ID, null);

            HttpEntity resEntity;

            HttpClient client = new DefaultHttpClient();
            VerifySMS obj = new VerifySMS(smsCode, deviceId);
            Gson gson = new Gson();
            final String json = gson.toJson(obj);
            System.out.println("ValidatePasswordJob CHECKING json " + json);
            String url = "http://209.10.93.234:8080/babylon/ws/verifySMS";
            HttpPost post = new HttpPost(url);
            StringEntity postingString = null;//gson.tojson() converts your pojo to json

            postingString = new StringEntity(json);
            post.setEntity(postingString);

            post.setHeader("Content-Type", "application/json");


            String response_str = null;

            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            System.out.println("ValidatePasswordJob CHECKING response_str " + response_str);


            if (response_str != null) {
                EventBus.getDefault().post(new VerifySMSJobCompletedEvent(response_str));
            } else {
                EventBus.getDefault().post(new VerifySMSJobCompletedEvent(null));
            }



        } catch (Exception e) {
            System.out.println("ValidatePasswordJob CHECKING Exception " + e);

            EventBus.getDefault().post(new VerifySMSJobCompletedEvent(null));
        }

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


}
