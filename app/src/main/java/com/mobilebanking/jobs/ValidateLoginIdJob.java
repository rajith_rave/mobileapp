package com.mobilebanking.jobs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.mobilebanking.Constants;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SignUpJobCompletedEvent;
import com.mobilebanking.events.ValidateLoginIdJobCompletedEvent;
import com.mobilebanking.pojo.Login;
import com.mobilebanking.pojo.SignUp;
import com.mobilebanking.util;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 01/May/2017.
 */

public class ValidateLoginIdJob extends Job {
    private static final String TAG = "ValidateLoginIdJob";
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private static final long serialVersionUID = 1L;
    private final int id;


    private final String latitute;
    private final String longitude;
    private final String loginId;


    private static final int PRIORITY = 1;


    public ValidateLoginIdJob(String loginId, String latitute, String longitude) {
        super(new Params(PRIORITY).persist().requireNetwork());

        id = jobCounter.incrementAndGet();
        this.latitute = latitute;
        this.longitude = longitude;
        this.loginId = loginId;

        System.out.println("ValidateLoginIdJob CHECKING firstName" + loginId);
        System.out.println("ValidateLoginIdJob CHECKING lastName" + latitute);
        System.out.println("ValidateLoginIdJob CHECKING email" + longitude);

    }


    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {

        Log.d(TAG, "onRun start");

        if (id != jobCounter.get()) {


            Log.d(TAG, "onRun id != jobCounter.get()");

            return;
        }


        try {

            System.out.println("ValidateLoginIdJob CHECKING try");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
            final SharedPreferences.Editor editor = preferences.edit();
            String deviceId = preferences.getString(Constants.DEVICE_ID, null);

            HttpEntity resEntity;

            HttpClient client = new DefaultHttpClient();
            Login obj = new Login(loginId, "mbanking", "6.9271", deviceId, "79.8612");
            Gson gson = new Gson();
            final String json = gson.toJson(obj);
            System.out.println("ValidateLoginIdJob CHECKING json " + json);
            String url = "http://209.10.93.234:8080/babylon/ws/validateLoginID";
            HttpPost post = new HttpPost(url);
            StringEntity postingString = null;//gson.tojson() converts your pojo to json

            postingString = new StringEntity(json);
            post.setEntity(postingString);

            post.setHeader("Content-Type", "application/json");


            String response_str = null;

            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            System.out.println("ValidateLoginIdJob CHECKING response_str " + response_str);


            if (response_str != null) {
                EventBus.getDefault().post(new ValidateLoginIdJobCompletedEvent(response_str));
            } else {
                EventBus.getDefault().post(new ValidateLoginIdJobCompletedEvent(null));
            }



        } catch (Exception e) {
            System.out.println("ValidateLoginIdJob CHECKING Exception " + e);

            EventBus.getDefault().post(new ValidateLoginIdJobCompletedEvent(null));
        }

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


}
