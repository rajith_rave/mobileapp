package com.mobilebanking.jobs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.mobilebanking.Constants;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.UpdateLocationJobCompletedEvent;
import com.mobilebanking.events.ValidateLoginIdJobCompletedEvent;
import com.mobilebanking.pojo.Login;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;

/**
 * Created by Tharindu on 5/2/2017.
 */

public class UpdateLocationJob extends Job {
    private static final String TAG = "UpdateLocationJob";
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private static final long serialVersionUID = 1L;
    private final int id;


    private final String json;


    private static final int PRIORITY = 1;


    public UpdateLocationJob(String json) {
        super(new Params(PRIORITY).persist().requireNetwork());

        id = jobCounter.incrementAndGet();
        this.json = json;

        System.out.println("UpdateLocationJob CHECKING json" + json);
    }


    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {

        Log.d(TAG, "onRun start");

        if (id != jobCounter.get()) {


            Log.d(TAG, "onRun id != jobCounter.get()");

            return;
        }


        try {

            System.out.println("UpdateLocationJob CHECKING try");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
            final SharedPreferences.Editor editor = preferences.edit();
            String deviceId = preferences.getString(Constants.DEVICE_ID, null);

            HttpEntity resEntity;

            HttpClient client = new DefaultHttpClient();
            System.out.println("UpdateLocationJob CHECKING json " + json);
            String url = "http://209.10.93.234:8080/babylon/ws/updateMyLocationSettings";
            HttpPut httpPUT = new HttpPut(url);
            StringEntity postingString = null;//gson.tojson() converts your pojo to json

            postingString = new StringEntity(json);
            httpPUT.setEntity(postingString);

            httpPUT.setHeader("Content-Type", "application/json");


            String response_str = null;

            HttpResponse response = client.execute(httpPUT);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            System.out.println("UpdateLocationJob CHECKING response_str " + response_str);


            if (response_str != null) {
                EventBus.getDefault().post(new UpdateLocationJobCompletedEvent(response_str));
            } else {
                EventBus.getDefault().post(new UpdateLocationJobCompletedEvent(null));
            }



        } catch (Exception e) {
            System.out.println("UpdateLocationJob CHECKING Exception " + e);

            EventBus.getDefault().post(new UpdateLocationJobCompletedEvent(null));
        }

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


}
