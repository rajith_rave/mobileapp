package com.mobilebanking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.services.ConnectivityReceiver;
import com.path.android.jobqueue.JobManager;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class BiometricSecurityActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener,CompoundButton.OnCheckedChangeListener{

    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private RadioButton rb4;
    private RadioButton rb5;
    private Button nextButton;
    private Button skipButton;
    private ProgressDialog progress;
    private JobManager jobManager;
    private ImageView backIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avtivity_biometric_security);
        Window window = BiometricSecurityActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(BiometricSecurityActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        rb1 = (RadioButton) findViewById(R.id.fingerprint);
        rb2 = (RadioButton) findViewById(R.id.voice);
        rb3 = (RadioButton) findViewById(R.id.face);
        rb4 = (RadioButton) findViewById(R.id.iris);
        rb5 = (RadioButton) findViewById(R.id.otp);

        rb1.setOnCheckedChangeListener(BiometricSecurityActivity.this);
        rb2.setOnCheckedChangeListener(BiometricSecurityActivity.this);
        rb3.setOnCheckedChangeListener(BiometricSecurityActivity.this);
        rb4.setOnCheckedChangeListener(BiometricSecurityActivity.this);
        rb5.setOnCheckedChangeListener(BiometricSecurityActivity.this);
        nextButton  = (Button) findViewById(R.id.next_button);
        skipButton  = (Button) findViewById(R.id.skip_button);
        backIcon  = (ImageView) findViewById(R.id.back_image);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(rb1.isChecked() || rb2.isChecked() || rb3.isChecked() || rb4.isChecked() || rb5.isChecked()){
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    if (rb1.isChecked()) {
                        editor.putInt(Constants.SELECTED_SECURITY_TYPE, 1);

                        editor.commit();
                    } else if (rb2.isChecked()) {
                        editor.putInt(Constants.SELECTED_SECURITY_TYPE, 2);

                        editor.commit();
                    } else if (rb3.isChecked()) {
                        editor.putInt(Constants.SELECTED_SECURITY_TYPE, 3);

                        editor.commit();
                    } else if (rb4.isChecked()) {
                        editor.putInt(Constants.SELECTED_SECURITY_TYPE, 4);

                        editor.commit();
                    } else if (rb5.isChecked()) {
                        editor.putInt(Constants.SELECTED_SECURITY_TYPE, 5);

                        editor.commit();
                    }

                    Intent i = new Intent(BiometricSecurityActivity.this, FingeprintActivity.class);
                    startActivity(i);

                }else{
                    Toast toast = Toast.makeText(BiometricSecurityActivity.this,
                            "Please choose one security type you want to set biometric security", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }




            }
        });
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(BiometricSecurityActivity.this, SelectLocationActivity.class);
                startActivity(i);



            }
        });

    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.fingerprint:
                if (rb1.isChecked() && b) {
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb2.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.voice:
                if (rb2.isChecked() && b) {
                    rb1.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.face:
                if (rb3.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb2.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.iris:
                if (rb4.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb5.setChecked(false);

                    return;

                }
                    rb1.setEnabled(true);
                    rb2.setEnabled(true);
                    rb3.setEnabled(true);
                    rb5.setEnabled(true);



                break;
            case R.id.otp:
                if (rb5.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb2.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);

                break;
            default:
                break;
        }
    }
}