package com.mobilebanking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.ValidateLoginIdJobCompletedEvent;
import com.mobilebanking.events.ValidatePasswordJobCompletedEvent;
import com.mobilebanking.jobs.ValidateLoginIdJob;
import com.mobilebanking.jobs.ValidatePasswordJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2015.
 */

public class PasswordActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private EditText passwordEditext;
    private Button loginButton;

    private String password;
    private ProgressDialog progress;
    private JobManager jobManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        Window window = PasswordActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(PasswordActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        passwordEditext = (EditText) findViewById(R.id.password_edittext);
        loginButton  = (Button) findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = String.valueOf(passwordEditext.getText());
                validateData(password);



            }
        });


    }

    private void validateData(final String password) {
        boolean isValidData = true;
        if (!password.isEmpty()) {

        } else {
            passwordEditext.setError("Required feild");
            isValidData = false;
        }


        if (isValidData) {

            if (checkConnection()) {

                progress = new ProgressDialog(PasswordActivity.this);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setMessage("Loading");
                progress.show();

                loginJob(password);

            } else {
                if (progress != null)
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                Toast toast = Toast.makeText(PasswordActivity.this,
                        "No internet connection. Please try again later", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }


        } else {
            Toast toast = Toast.makeText(PasswordActivity.this,
                    "Please fill the feilds and try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }


    }
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void loginJob( String password) {


        jobManager.addJobInBackground(new ValidatePasswordJob(password));
    }


    public void onEventBackgroundThread(final ValidatePasswordJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {
                            int security_type_id = resultJsonObject.getInt("security_type_id");
                            int security_status = resultJsonObject.getInt("security_status");
                            String firstName = resultJsonObject.getString("first_name");
                            String lastName = resultJsonObject.getString("last_name");


                            editor.putInt(Constants.SECURITY_TYPE_ID, security_type_id);
                            editor.putInt(Constants.SECURITY_STATUS, security_status);
                            editor.putString(Constants.FIRST_NAME, firstName);
                            editor.putString(Constants.LAST_NAME, lastName);
                            editor.commit();

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            boolean iseBankingSelected = preferences.getBoolean(Constants.E_BANKING_CHECKBOX,false);
                            boolean ismBankingSelected = preferences.getBoolean(Constants.M_BANKING_CHECKBOX,false);
                            boolean isatmBankingSelected = preferences.getBoolean(Constants.ATM_CHECKBOX,false);

                            if(iseBankingSelected || ismBankingSelected || isatmBankingSelected){
                                Intent i = new Intent(PasswordActivity.this, FingerPrintVerificationActivity.class);
                                startActivity(i);
                                finish();
                            }else{
                                Intent i = new Intent(PasswordActivity.this, DashboardActivity.class);
                                startActivity(i);
                                finish();
                            }


                        } else {
                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    statusMessage, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(PasswordActivity.this);
                    }

                } else {
                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(PasswordActivity.this);

                }

            }
        });

    }
}