package com.mobilebanking.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.UpdateSecurityJobCompletedEvent;
import com.mobilebanking.jobs.UpdateSecurityJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Tharindu on 5/9/2017.
 */

public class UpdateSecurityActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener,CompoundButton.OnCheckedChangeListener{

    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private RadioButton rb4;
    private RadioButton rb5;
    private Button submitButton;
    private ProgressDialog progress;
    private JobManager jobManager;
    private ImageView backImageview;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_security_activity);
        Window window = UpdateSecurityActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(UpdateSecurityActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        backImageview = (ImageView) findViewById(R.id.back_image);
        rb1 = (RadioButton)findViewById(R.id.fingerprint);
        rb2 = (RadioButton) findViewById(R.id.voice);
        rb3 = (RadioButton) findViewById(R.id.face);
        rb4 = (RadioButton) findViewById(R.id.iris);
        rb5 = (RadioButton) findViewById(R.id.otp);

        rb1.setOnCheckedChangeListener(this);
        rb2.setOnCheckedChangeListener(this);
        rb3.setOnCheckedChangeListener(this);
        rb4.setOnCheckedChangeListener(this);
        rb5.setOnCheckedChangeListener(this);
        submitButton  = (Button) findViewById(R.id.submit_button);
        backImageview = (ImageView) findViewById(R.id.back_image);
        backImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
        final SharedPreferences.Editor editor = preferences.edit();

        int security_type_id = preferences.getInt(Constants.SELECTED_SECURITY_TYPE, 0);
        if(security_type_id == 0){
            System.out.println("checking final json 33333333333 ");

        }else{
            if(security_type_id == 1){
                rb1.setChecked(true);
            }else if(security_type_id == 2){
                rb2.setChecked(true);
            }else if(security_type_id == 3){
                rb3.setChecked(true);
            }else if(security_type_id == 4){
                rb4.setChecked(true);
            }else if(security_type_id == 5){
                rb5.setChecked(true);
            }
            System.out.println("checking final json 4444444444 ");

        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                final SharedPreferences.Editor editor = preferences.edit();
                if (rb1.isChecked()) {
                    editor.putInt(Constants.SELECTED_SECURITY_TYPE, 1);

                    editor.commit();
                } else if (rb2.isChecked()) {
                    editor.putInt(Constants.SELECTED_SECURITY_TYPE, 2);

                    editor.commit();
                } else if (rb3.isChecked()) {
                    editor.putInt(Constants.SELECTED_SECURITY_TYPE, 3);

                    editor.commit();
                } else if (rb4.isChecked()) {
                    editor.putInt(Constants.SELECTED_SECURITY_TYPE, 4);

                    editor.commit();
                } else if (rb5.isChecked()) {
                    editor.putInt(Constants.SELECTED_SECURITY_TYPE, 5);

                    editor.commit();
                }
                int security_type_id = preferences.getInt(Constants.SELECTED_SECURITY_TYPE, 0);
                String deviceId = preferences.getString(Constants.DEVICE_ID, null);

                try {
                    System.out.println("checking final json 22222222 ");

                    JSONObject jsonObjSend = new JSONObject();
                    jsonObjSend.put("device_id", deviceId);

                    JSONArray arr = new JSONArray();
                    JSONObject productIdOneObject = new JSONObject();
                    productIdOneObject.put("my_product_id", 7);
                    productIdOneObject.put("product_name", "e-banking");
                    productIdOneObject.put("security_type_id", security_type_id);
                    boolean iseBankingSelected = preferences.getBoolean(Constants.E_BANKING_CHECKBOX,false);
                    System.out.println("checking final json 555555555 " + iseBankingSelected);

                    if(iseBankingSelected){
                        productIdOneObject.put("status", 1);

                    }else{
                        productIdOneObject.put("status", 2);

                    }
                    arr.put(productIdOneObject);
                    JSONObject productIdTwoObject = new JSONObject();
                    productIdTwoObject.put("my_product_id", 8);
                    productIdTwoObject.put("product_name", "m-banking");
                    productIdTwoObject.put("security_type_id", security_type_id);
                    boolean ismBankingSelected = preferences.getBoolean(Constants.M_BANKING_CHECKBOX,false);
                    System.out.println("checking final json 666666666666 " + ismBankingSelected);

                    if(ismBankingSelected){
                        productIdTwoObject.put("status", 1);

                    }else{
                        productIdTwoObject.put("status", 2);

                    }

                    arr.put(productIdTwoObject);

                    JSONObject productIdThreeObject = new JSONObject();
                    productIdThreeObject.put("my_product_id", 9);
                    productIdThreeObject.put("product_name", "ATM-banking");
                    productIdThreeObject.put("security_type_id", security_type_id);
                    boolean isatmBankingSelected = preferences.getBoolean(Constants.ATM_CHECKBOX,false);
                    System.out.println("checking final json 77777777777 " + isatmBankingSelected);
                    if(isatmBankingSelected){
                        productIdThreeObject.put("status", 1);

                    }else{
                        productIdThreeObject.put("status", 2);

                    }

                    arr.put(productIdThreeObject);

                    jsonObjSend.put("my_products", arr);

                    System.out.println("checking final json 888888888 " + jsonObjSend.toString());
                    progress = new ProgressDialog(UpdateSecurityActivity.this);
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setMessage("Loading");
                    progress.show();
                    updateSecurityJob(jsonObjSend.toString());



                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("checking final json 99999999999 " + e);

                }






            }
        });


    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.fingerprint:
                if (rb1.isChecked() && b) {
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb2.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.voice:
                if (rb2.isChecked() && b) {
                    rb1.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.face:
                if (rb3.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb4.setChecked(false);
                    rb5.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb2.setEnabled(true);
                rb4.setEnabled(true);
                rb5.setEnabled(true);

                break;
            case R.id.iris:
                if (rb4.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb5.setChecked(false);

                    return;

                }
                rb1.setEnabled(true);
                rb2.setEnabled(true);
                rb3.setEnabled(true);
                rb5.setEnabled(true);



                break;
            case R.id.otp:
                if (rb5.isChecked() && b) {
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb4.setChecked(false);

                    return;
                }

                rb1.setEnabled(true);
                rb2.setEnabled(true);
                rb3.setEnabled(true);
                rb4.setEnabled(true);

                break;
            default:
                break;
        }
    }

    private void updateSecurityJob(String json) {


        jobManager.addJobInBackground(new UpdateSecurityJob(json));
    }

    public void onEventBackgroundThread(final UpdateSecurityJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    System.out.println("UpdateSecurityJobCompletedEvent 1111111111 ");

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        System.out.println("UpdateSecurityJobCompletedEvent 2222222222 ");


                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {
                            System.out.println("UpdateSecurityJobCompletedEvent 333333333333333 ");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(UpdateSecurityActivity.this,
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {
                            System.out.println("UpdateSecurityJobCompletedEvent 44444444444444444 ");


                            System.out.println("UpdateSecurityJobCompletedEvent 555555555555 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            showAlertDialog(statusMessage);
                            //                            Intent i = new Intent(SelectLocationActivity.this, SmsVerificationActivity.class);
//                            startActivity(i);

                        } else {

                            System.out.println("UpdateSecurityJobCompletedEvent 66666666666666 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Toast toast = Toast.makeText(UpdateSecurityActivity.this,
                                    statusMessage, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                        System.out.println("UpdateSecurityJobCompletedEvent 777777777 " + e);

                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(UpdateSecurityActivity.this);
                    }

                } else {
                    System.out.println("UpdateSecurityJobCompletedEvent 88888888888 ");

                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(UpdateSecurityActivity.this);

                }

            }
        });

    }

    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(UpdateSecurityActivity.this);
        final View dialogView = myLayout.inflate(R.layout.submit_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                UpdateSecurityActivity.this);
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UpdateSecurityActivity.this, LoginActivity.class);
                startActivity(i);
                finishAffinity();
            }
        });
    }
}