package com.mobilebanking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.ValidateLoginIdJobCompletedEvent;
import com.mobilebanking.events.VerifySMSJobCompletedEvent;
import com.mobilebanking.jobs.ValidateLoginIdJob;
import com.mobilebanking.jobs.VerifySMSJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2015.
 */

public class SmsVerificationActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private EditText smsCodeEditext;
    private Button verifyButton;
    private ImageView backIcon;

    private String smsCode;
    private ProgressDialog progress;
    private JobManager jobManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_verification);
        Window window = SmsVerificationActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(SmsVerificationActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        smsCodeEditext = (EditText) findViewById(R.id.sms_code_edittext);
        verifyButton  = (Button) findViewById(R.id.verify_button);
        backIcon  = (ImageView) findViewById(R.id.back_image);

        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smsCode = String.valueOf(smsCodeEditext.getText());
                validateData(smsCode);



            }
        });
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();


            }
        });


    }

    private void validateData(final String smsCode) {
        boolean isValidData = true;
        if (!smsCode.isEmpty()) {

        } else {
            smsCodeEditext.setError("Required feild");
            isValidData = false;
        }


        if (isValidData) {

            if (checkConnection()) {

                progress = new ProgressDialog(SmsVerificationActivity.this);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setMessage("Loading");
                progress.show();

                smsJob(smsCode);

            } else {
                if (progress != null)
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                Toast toast = Toast.makeText(SmsVerificationActivity.this,
                        "No internet connection. Please try again later", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }


        } else {
            Toast toast = Toast.makeText(SmsVerificationActivity.this,
                    "Please fill the feilds and try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }


    }
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void smsJob( String smsCode) {


        jobManager.addJobInBackground(new VerifySMSJob(smsCode));
    }

    public void onEventBackgroundThread(final VerifySMSJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        JSONObject statusDeoObject = resultJsonObject.getJSONObject("status_dto");
                        int statusCode = statusDeoObject.getInt("status_code");
                        final String statusMessage = statusDeoObject.getString("status_message");
                        if (statusCode == 2) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {

                            JSONArray productListArray = resultJsonObject.getJSONArray("product_list");



//                            String deviceId = resultJsonObject.getString("device_id");
//                            String userId = resultJsonObject.getString("user_id");
//
//                            editor.putString(Constants.LOGIN_ID, loginId);
//                            editor.putString(Constants.DEVICE_ID, deviceId);
//                            editor.putString(Constants.USER_ID, userId);
                            editor.commit();

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Intent i = new Intent(SmsVerificationActivity.this, ProductsListActivity.class);
                            startActivity(i);

                        } else {
                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    statusMessage, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(SmsVerificationActivity.this);
                    }

                } else {
                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(SmsVerificationActivity.this);

                }

            }
        });

    }
}