package com.mobilebanking.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SignUpJobCompletedEvent;
import com.mobilebanking.jobs.SignUpJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2015.
 */

public class SignUpActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private EditText firstnameEditext;
    private EditText lastnameEditext;
    private EditText loginIdEditext;
    private EditText passwordEditext;
    private EditText mobileNumberEditext;
    private EditText emailEditext;
    private EditText deviceIdEditext;
    private Button signUpButton;
    private ImageView informationIcon;
    private ImageView backImageview;
    private String loginId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String deviceId;
    private String mobileNumber;
    private ProgressDialog progress;
    private JobManager jobManager;
    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Window window = SignUpActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(SignUpActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        firstnameEditext = (EditText) findViewById(R.id.f_name_edittext);
        lastnameEditext = (EditText) findViewById(R.id.l_name_edittext);
        loginIdEditext = (EditText) findViewById(R.id.login_id_edittext);
        passwordEditext = (EditText) findViewById(R.id.password_edittext);
        mobileNumberEditext = (EditText) findViewById(R.id.mobile_number_edittext);
        emailEditext = (EditText) findViewById(R.id.email_edittext);
        deviceIdEditext = (EditText) findViewById(R.id.device_id_edittext);
        signUpButton = (Button) findViewById(R.id.signup_button);
        informationIcon = (ImageView) findViewById(R.id.information_icon_imageview);
        backImageview = (ImageView) findViewById(R.id.back_image);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginId = String.valueOf(loginIdEditext.getText());
                firstName = String.valueOf(firstnameEditext.getText());
                lastName = String.valueOf(lastnameEditext.getText());
                email = String.valueOf(emailEditext.getText());
                password = String.valueOf(passwordEditext.getText());
                deviceId = String.valueOf(deviceIdEditext.getText());
                System.out.println("SignUpJob CHECKING activity  deviceId   " + deviceId);
                mobileNumber = String.valueOf(mobileNumberEditext.getText());
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                final SharedPreferences.Editor editor = preferences.edit();
                String deviceeeId = preferences.getString(Constants.DEVICE_ID, null);
                System.out.println("SignUpJob CHECKING activity  deviceeeId   " + deviceeeId);

                if(deviceeeId!=null){
                    System.out.println("SignUpJob CHECKING activity  dgdggdgg   " + deviceId);

                    Toast toast = Toast.makeText(SignUpActivity.this,
                            "This device already registered. please uninstall the app and try again ", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }else{
                    System.out.println("SignUpJob CHECKING activity  22222222222   " + deviceId);

                    validateData(loginId, firstName, lastName, email, password, deviceId, mobileNumber);

                }


            }
        });

        informationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog("This is to uniquely identify your device");

            }
        });
        backImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

    }

    private void validateData(final String loginId, final String firstName, final String lastName, final String email, final String password, final String deviceId, final String mobileNumber) {
        System.out.println("SignUpJob CHECKING activity  3333333333   " + deviceId);


        boolean isValidData = true;
        if (!loginId.isEmpty()) {

        } else {
            loginIdEditext.setError("Required feild");
            isValidData = false;
        }
        if (!firstName.isEmpty()) {

        } else {
            firstnameEditext.setError("Required feild");

            isValidData = false;
        }
        if (!lastName.isEmpty()) {

        } else {
            lastnameEditext.setError("Required feild");
            isValidData = false;
        }

        if (!email.isEmpty()) {
            if (checkEmail(email)) {

            } else {
                if (isValidEmail(email)) {

                } else {
                    emailEditext.setError("Invalid email");

                    isValidData = false;
                }

            }
        } else {
            emailEditext.setError("Required feild");

            isValidData = false;
        }

        if (!password.isEmpty()) {
            if (password.length() > 5) {
            } else {
               passwordEditext.setError("Password should contains minimam 6 characters");
                isValidData = false;
            }
        } else {
            passwordEditext.setError("Required Field");
            isValidData = false;
        }

        if (isValidData) {

            if (checkConnection()) {

                progress = new ProgressDialog(SignUpActivity.this);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setMessage("Loading");
                progress.show();
                System.out.println("SignUpJob CHECKING activity  44444444444444   " + deviceId);

                signUpJob(loginId, firstName, lastName, email, password, deviceId, mobileNumber);

            } else {
                if (progress != null)
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                Toast toast = Toast.makeText(SignUpActivity.this,
                        "No internet connection. Please try again later", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }


        } else {
            Toast toast = Toast.makeText(SignUpActivity.this,
                    "Please fill the feilds and try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }


    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}+"
    );

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void signUpJob( String loginId,  String firstName,  String lastName,  String email,  String password,  String deviceId,  String mobileNumber) {

        System.out.println("SignUpJob CHECKING activity  55555555555   " + deviceId);

        jobManager.addJobInBackground(new SignUpJob(loginId, firstName, lastName, email, password, deviceId, mobileNumber));
    }

    public void onEventBackgroundThread(final SignUpJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    System.out.println("SignUpJobCompletedEvent 1111111111 ");

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        System.out.println("SignUpJobCompletedEvent 2222222222 ");


                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {
                            System.out.println("SignUpJobCompletedEvent 333333333333333 ");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {
                            System.out.println("SignUpJobCompletedEvent 44444444444444444 ");

                            editor.putString(Constants.FIRST_NAME, firstName);
                            editor.putString(Constants.LAST_NAME, lastName);
                            editor.putString(Constants.EMAIL, email);
                            editor.putString(Constants.LOGIN_ID, loginId);
                            editor.putString(Constants.MOBILE_NUMBER, mobileNumber);
                            editor.putString(Constants.DEVICE_ID, deviceId);
                            editor.commit();
                            System.out.println("SignUpJobCompletedEvent 555555555555 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Intent i = new Intent(SignUpActivity.this, SmsVerificationActivity.class);
                            startActivity(i);

                        } else {

                            System.out.println("SignUpJobCompletedEvent 66666666666666 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            util.errorMessage(SignUpActivity.this);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                        System.out.println("SignUpJobCompletedEvent 777777777 " + e);

                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(SignUpActivity.this);
                    }

                } else {
                    System.out.println("SignUpJobCompletedEvent 88888888888 ");

                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(SignUpActivity.this);

                }

            }
        });

    }

    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(SignUpActivity.this);
        final View dialogView = myLayout.inflate(R.layout.submit_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SignUpActivity.this);
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

}