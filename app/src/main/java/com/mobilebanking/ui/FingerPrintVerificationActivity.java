package com.mobilebanking.ui;

import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.mobilebanking.R;
import com.multidots.fingerprintauth.AuthErrorCodes;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;
import com.multidots.fingerprintauth.FingerPrintUtils;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class FingerPrintVerificationActivity extends AppCompatActivity implements FingerPrintAuthCallback {

    private TextView mAuthMsgTv;
    private TextView fingerTextView;
    private TextView greetingTextView;
    private ImageView downarrow;
    private ViewSwitcher mSwitcher;
    private Button mGoToSettingsBtn;
    private FingerPrintAuthHelper mFingerPrintAuthHelper;
    private RelativeLayout successLayout;
    private RelativeLayout failedLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print_verification);

        mGoToSettingsBtn = (Button) findViewById(R.id.go_to_settings_btn);
        successLayout = (RelativeLayout) findViewById(R.id.access_granted_layout);
        failedLayout = (RelativeLayout) findViewById(R.id.access_deniot_layout);
        mGoToSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FingerPrintUtils.openSecuritySettings(FingerPrintVerificationActivity.this);
            }
        });
        Window window = FingerPrintVerificationActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(FingerPrintVerificationActivity.this, R.color.orange));
        }

        downarrow = (ImageView) findViewById(R.id.dwon);
        mSwitcher = (ViewSwitcher) findViewById(R.id.main_switcher);
        mAuthMsgTv = (TextView) findViewById(R.id.auth_message_tv);
        fingerTextView = (TextView) findViewById(R.id.inger_textciew);
        greetingTextView = (TextView) findViewById(R.id.invite_code_textView);

        EditText pinEt = (EditText) findViewById(R.id.pin_et);
        pinEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("1234")){

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoToSettingsBtn.setVisibility(View.GONE);
        downarrow.setVisibility(View.VISIBLE);
fingerTextView.setVisibility(View.VISIBLE);

        //start finger print authentication
        mFingerPrintAuthHelper.startAuth();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFingerPrintAuthHelper.stopAuth();
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        mAuthMsgTv.setText("Your device does not have finger print scanner. Please type 1234 to authenticate.");
        mSwitcher.showNext();
    }

    @Override
    public void onNoFingerPrintRegistered() {
        mAuthMsgTv.setText("There are no finger prints registered on this device. Please register your finger from settings.");
        mGoToSettingsBtn.setVisibility(View.VISIBLE);
        downarrow.setVisibility(View.GONE);
        fingerTextView.setVisibility(View.GONE);

    }

    @Override
    public void onBelowMarshmallow() {
        mAuthMsgTv.setText("You are running older version of android that does not support finger print authentication.");
        mSwitcher.showNext();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        successLayout.setVisibility(View.VISIBLE);
        failedLayout.setVisibility(View.GONE);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(FingerPrintVerificationActivity.this, DashboardActivity.class));
                        finishAffinity();
                    }
                }, 1000);

    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        switch (errorCode) {
            case AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                successLayout.setVisibility(View.GONE);
                failedLayout.setVisibility(View.VISIBLE);
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                failedLayout.setVisibility(View.GONE);

                            }
                        }, 500);
                break;
            case AuthErrorCodes.NON_RECOVERABLE_ERROR:
//                mAuthMsgTv.setText("Cannot initialize finger print authentication. Please type 1234 to authenticate.");
//                mSwitcher.showNext();
                break;
            case AuthErrorCodes.RECOVERABLE_ERROR:
                mAuthMsgTv.setText(errorMessage);
                break;
        }
    }
}
