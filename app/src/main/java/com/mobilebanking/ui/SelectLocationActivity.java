package com.mobilebanking.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SetSecurityTypesJobCompletedEvent;
import com.mobilebanking.events.SignUpJobCompletedEvent;
import com.mobilebanking.events.VerifySMSJobCompletedEvent;
import com.mobilebanking.jobs.SetSecurityTypesJob;
import com.mobilebanking.jobs.VerifySMSJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;
import com.path.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class SelectLocationActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{


    private EditText locationEditText;
    private Button submitButton;
    private CountryPicker mCountryPicker;
    private String country = "Any Location";
    private ProgressDialog progress;
    private JobManager jobManager;
    private ImageView backIcon;
    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        Window window = SelectLocationActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(SelectLocationActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }

        locationEditText  = (EditText) findViewById(R.id.county_edittext);
        submitButton  = (Button) findViewById(R.id.submit_button);
        backIcon  = (ImageView) findViewById(R.id.back_image);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });
        mCountryPicker = CountryPicker.newInstance("Select Country");
        setListener();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("checking final json 11111111111111 ");
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                final SharedPreferences.Editor editor = preferences.edit();

                String deviceId = preferences.getString(Constants.DEVICE_ID, null);
               country = String.valueOf(locationEditText.getText());
int security_type_id = preferences.getInt(Constants.SELECTED_SECURITY_TYPE, 0);
                try {
                    System.out.println("checking final json 22222222 ");

                    JSONObject jsonObjSend = new JSONObject();
                    jsonObjSend.put("device_id", deviceId);
                    jsonObjSend.put("location", country);
                    if(security_type_id == 0){
                        System.out.println("checking final json 33333333333 ");
                        jsonObjSend.put("security_type_id", 1);

                    }else{
                        jsonObjSend.put("security_type_id", security_type_id);
                        System.out.println("checking final json 4444444444 ");

                    }

                    JSONArray arr = new JSONArray();
                    JSONObject productIdOneObject = new JSONObject();
                    productIdOneObject.put("product_id", 1);
                    boolean iseBankingSelected = preferences.getBoolean(Constants.E_BANKING_CHECKBOX,false);
                    System.out.println("checking final json 555555555 " + iseBankingSelected);

                    if(iseBankingSelected){
                        productIdOneObject.put("security_status", 1);

                    }else{
                        productIdOneObject.put("security_status", 2);

                    }
                    arr.put(productIdOneObject);
                    JSONObject productIdTwoObject = new JSONObject();
                    productIdTwoObject.put("product_id", 2);
                    boolean ismBankingSelected = preferences.getBoolean(Constants.M_BANKING_CHECKBOX,false);
                    System.out.println("checking final json 666666666666 " + ismBankingSelected);

                    if(ismBankingSelected){
                        productIdTwoObject.put("security_status", 1);

                    }else{
                        productIdTwoObject.put("security_status", 2);

                    }

                    arr.put(productIdTwoObject);

                    JSONObject productIdThreeObject = new JSONObject();
                    productIdThreeObject.put("product_id", 3);
                    boolean isatmBankingSelected = preferences.getBoolean(Constants.ATM_CHECKBOX,false);
                    System.out.println("checking final json 77777777777 " + isatmBankingSelected);
                    if(isatmBankingSelected){
                        productIdThreeObject.put("security_status", 1);

                    }else{
                        productIdThreeObject.put("security_status", 2);

                    }

                    arr.put(productIdThreeObject);

                    jsonObjSend.put("products", arr);

                    System.out.println("checking final json 888888888 " + jsonObjSend.toString());
                    progress = new ProgressDialog(SelectLocationActivity.this);
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setMessage("Loading");
                    progress.show();

                    securityJob(jsonObjSend.toString());



                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("checking final json 99999999999 " + e);

                }




            }
        });


    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                locationEditText.setText(name);
               country=name;
                mCountryPicker.dismiss();
            }
        });
        locationEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

    }

    private void securityJob(String json) {


        jobManager.addJobInBackground(new SetSecurityTypesJob(json));
    }
    public void onEventBackgroundThread(final SetSecurityTypesJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    System.out.println("SetSecurityTypesJobCompletedEvent 1111111111 ");

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        System.out.println("SetSecurityTypesJobCompletedEvent 2222222222 ");


                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {
                            System.out.println("SetSecurityTypesJobCompletedEvent 333333333333333 ");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {
                            System.out.println("SetSecurityTypesJobCompletedEvent 44444444444444444 ");


                            System.out.println("SetSecurityTypesJobCompletedEvent 555555555555 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            editor.putString(Constants.LOCATION, country);
                            editor.commit();
                            showAlertDialog(statusMessage);
//                            Intent i = new Intent(SelectLocationActivity.this, SmsVerificationActivity.class);
//                            startActivity(i);

                        } else {

                            System.out.println("SetSecurityTypesJobCompletedEvent 66666666666666 ");

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    statusMessage, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                        System.out.println("SetSecurityTypesJobCompletedEvent 777777777 " + e);

                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(SelectLocationActivity.this);
                    }

                } else {
                    System.out.println("SetSecurityTypesJobCompletedEvent 88888888888 ");

                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(SelectLocationActivity.this);

                }

            }
        });

    }

    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(SelectLocationActivity.this);
        final View dialogView = myLayout.inflate(R.layout.submit_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SelectLocationActivity.this);
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SelectLocationActivity.this, LoginActivity.class);
                            startActivity(i);
                finishAffinity();
            }
        });
    }
}