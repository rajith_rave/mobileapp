package com.mobilebanking.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.ValidatePasswordJobCompletedEvent;
import com.mobilebanking.jobs.ValidatePasswordJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class ProductsListActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private CheckBox eBankingCheckBox;
    private CheckBox mBankingCheckBox;
    private CheckBox atmCheckBox;
    private Button nextButton;
    AlertDialog alertDialog;

    private String password;
    private ProgressDialog progress;
    private JobManager jobManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        Window window = ProductsListActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(ProductsListActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        eBankingCheckBox  = (CheckBox) findViewById(R.id.e_banking_checkbox);
        mBankingCheckBox  = (CheckBox) findViewById(R.id.m_banking_checkbox);
        atmCheckBox  = (CheckBox) findViewById(R.id.atm_checkbox);
        nextButton  = (Button) findViewById(R.id.next_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
if(eBankingCheckBox.isChecked() || mBankingCheckBox.isChecked() || atmCheckBox.isChecked()){
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
    final SharedPreferences.Editor editor = preferences.edit();
    if(eBankingCheckBox.isChecked()){
        editor.putBoolean(Constants.E_BANKING_CHECKBOX, true);

        editor.commit();
    }else{
        editor.putBoolean(Constants.E_BANKING_CHECKBOX, false);

        editor.commit();
    }

    if(mBankingCheckBox.isChecked()){
        editor.putBoolean(Constants.M_BANKING_CHECKBOX, true);

        editor.commit();
    }else{
        editor.putBoolean(Constants.M_BANKING_CHECKBOX, false);

        editor.commit();
    }
    if(atmCheckBox.isChecked()){
        editor.putBoolean(Constants.ATM_CHECKBOX, true);

        editor.commit();
    }else{
        editor.putBoolean(Constants.ATM_CHECKBOX, false);

        editor.commit();
    }
    if(eBankingCheckBox.isChecked()&&mBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){

        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication, m-Banking Mobile App and ATM Account?");
    }else if (eBankingCheckBox.isChecked()&&mBankingCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication and m-Banking Mobile App?");

    }else if (eBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication and ATM Account?");

    }else if (mBankingCheckBox.isChecked()&&atmCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the m-Banking Mobile App and ATM Account?");

    }else if (mBankingCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the m-Banking Mobile App?");

    }else if (atmCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the ATM Account?");

    }else if (eBankingCheckBox.isChecked()){
        showAlertDialog("Are you sure you want to enable biometric security for the e-Banking Apllication?");

    }

}else{
    Intent i = new Intent(ProductsListActivity.this, BiometricSecurityActivity.class);
    startActivity(i);
}








            }
        });


    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void showAlertDialog(String message) {
        LayoutInflater myLayout = LayoutInflater.from(ProductsListActivity.this);
        final View dialogView = myLayout.inflate(R.layout.conformation_alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProductsListActivity.this);
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        TextView okTextView = (TextView) alertDialog.findViewById(R.id.ok_textview);
        TextView noTextView = (TextView) alertDialog.findViewById(R.id.no_textview);
        TextView messageTextview = (TextView) alertDialog.findViewById(R.id.file_name_text_view);
        messageTextview.setText(message);
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                Intent i = new Intent(ProductsListActivity.this, BiometricSecurityActivity.class);
                startActivity(i);

            }
        });
        noTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }
}