package com.mobilebanking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.events.SignUpJobCompletedEvent;
import com.mobilebanking.events.ValidateLoginIdJobCompletedEvent;
import com.mobilebanking.jobs.SignUpJob;
import com.mobilebanking.jobs.ValidateLoginIdJob;
import com.mobilebanking.services.ConnectivityReceiver;
import com.mobilebanking.util;
import com.path.android.jobqueue.JobManager;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import io.realm.Realm;

/**
 * Created by Mugunthan on 30/Apr/2015.
 */

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private EditText loginIdEditext;
    private Button nextButton;
    private Button signUpButton;
    private String loginId;
    private ProgressDialog progress;
    private JobManager jobManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Window window = LoginActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(LoginActivity.this, R.color.orange));
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }
        loginIdEditext = (EditText) findViewById(R.id.login_id_edittext);
        nextButton  = (Button) findViewById(R.id.next_button);
        signUpButton  = (Button) findViewById(R.id.signup_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginId = String.valueOf(loginIdEditext.getText());

                validateData(loginId);


            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this,
                        SignUpActivity.class));


            }
        });
    }


    private void validateData(final String loginId) {
        boolean isValidData = true;
        if (!loginId.isEmpty()) {

        } else {
            loginIdEditext.setError("Required feild");
            isValidData = false;
        }


        if (isValidData) {

            if (checkConnection()) {

                progress = new ProgressDialog(LoginActivity.this);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setMessage("Loading");
                progress.show();

                loginJob(loginId);

            } else {
                if (progress != null)
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                Toast toast = Toast.makeText(LoginActivity.this,
                        "No internet connection. Please try again later", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }


        } else {
            Toast toast = Toast.makeText(LoginActivity.this,
                    "Please fill the feilds and try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }


    }
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return  isConnected;
    }

    @Override
    public void onResume() {
        MobileBankingApplication.getInstance().setConnectivityListener(this);
        super.onResume();
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void loginJob( String loginId) {


        jobManager.addJobInBackground(new ValidateLoginIdJob(loginId, "", ""));
    }

    public void onEventBackgroundThread(final ValidateLoginIdJobCompletedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.getResult() != null) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
                    final SharedPreferences.Editor editor = preferences.edit();
                    try {
                        JSONObject resultJsonObject = new JSONObject(event.getResult());
                        int statusCode = resultJsonObject.getInt("status_code");
                        final String statusMessage = resultJsonObject.getString("status_message");
                        if (statusCode == 2) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progress != null)
                                        if (progress.isShowing()) {
                                            progress.dismiss();
                                        }

                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            statusMessage, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }
                            });
                        } else if (statusCode == 1) {

                             String deviceId = resultJsonObject.getString("device_id");

                            editor.putString(Constants.LOGIN_ID, loginId);
                            editor.putString(Constants.DEVICE_ID, deviceId);
                            editor.commit();

                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Intent i = new Intent(LoginActivity.this, PasswordActivity.class);
                            startActivity(i);
                            finish();

                        } else {
                            if (progress != null)
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    statusMessage, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (progress != null)
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        util.errorMessage(LoginActivity.this);
                    }

                } else {
                    if (progress != null)
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    util.errorMessage(LoginActivity.this);

                }

            }
        });

    }
}