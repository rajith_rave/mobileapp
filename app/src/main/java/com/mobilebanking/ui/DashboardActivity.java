package com.mobilebanking.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobilebanking.Constants;
import com.mobilebanking.R;
import com.mobilebanking.application.MobileBankingApplication;
import com.mobilebanking.fragment.HomeFragment;
import com.mobilebanking.fragment.UpdateLocationFragment;
import com.mobilebanking.fragment.UpdateSecurityFragment;
import com.mobilebanking.services.ConnectivityReceiver;
import com.path.android.jobqueue.JobManager;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by Mugunthan on 30/Apr/2017.
 */

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private DrawerLayout drawer;
    private View mainContent;
    private RelativeLayout branchesLayout;
    private RelativeLayout alldealsLayout;
    private RelativeLayout signoutLayout;
    private RelativeLayout homeLayout;
    private RelativeLayout storesLayout;
    private RelativeLayout contactLayout;
    private RelativeLayout catergoryLayout;
    private RelativeLayout logoutLayout;
    private TextView mTitle;
    private ImageView search;
    private TextView editProfile;
    private Toolbar toolbar;
    private String selectedExchangeId;
    private String username;
    private JobManager jobManager;
    private  int screenSize;
    private CircleImageView profileImageView;
    private TextView usernameTextView;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Bundle extras;
    private boolean isConnected = false;
    private NetworkChangeReceiver receiver;
    private Snackbar snackbar;
    private ListView countryListView ;
    private ArrayAdapter<String> listAdapter ;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_dashboard);

        jobManager = MobileBankingApplication.getInstance().getJobManager();
        try {
            EventBus.getDefault().registerSticky(this);
        } catch (Throwable t) {
            // this may crash if registration did not go through. just be safe
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(MobileBankingApplication.getInstance());
        editor = preferences.edit();
        editor.putString(Constants.SIGN_IN_STATUS, Constants.SUCCESS);
        username = preferences.getString(Constants.FIRST_NAME, null) + " " + preferences.getString(Constants.LAST_NAME, null);
        editor.commit();
        extras = getIntent().getExtras();
        if (extras != null) {

        }else{

        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setText("HOME");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            System.out.println("cheking version");

            Window window = DashboardActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.orange));

        }

        if (checkConnection()) {


        } else {

            showSnack(false);
        }




//        SystemBarTintManager tintManager = new SystemBarTintManager(this);
//        // enable status bar tint
//        tintManager.setStatusBarTintEnabled(true);
//        // enable navigation bar tint
//        tintManager.setNavigationBarTintEnabled(true);
//        tintManager.setTintColor(Color.parseColor("#ffffff"));
        mainContent = findViewById(R.id.main_content);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {

            @Override
            public void onDrawerSlide(View drawer, float slideOffset) {
                super.onDrawerSlide(drawer, slideOffset);
                mainContent.setTranslationX(slideOffset * drawer.getWidth());


            }

            public void onDrawerClosed(View view) {
            }

            public void onDrawerOpened(View drawerView) {

            }
        };
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.nav_icon, null);
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setFragment(new HomeFragment());

        screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;


        switch(screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                toolbar.setBackgroundResource(R.drawable.all_deals_header);
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                toolbar.setBackgroundResource(R.drawable.all_deals_header);
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:

                break;
            default:

        }



        branchesLayout = (RelativeLayout) navigationView.findViewById(R.id.branches_layout);
        branchesLayout.setOnClickListener(this);

        homeLayout = (RelativeLayout) navigationView.findViewById(R.id.home_layout);
        homeLayout.setOnClickListener(this);


        alldealsLayout = (RelativeLayout) navigationView.findViewById(R.id.alldeals_layout);
        alldealsLayout.setOnClickListener(this);

        signoutLayout = (RelativeLayout) navigationView.findViewById(R.id.signout_layout);
        signoutLayout.setOnClickListener(this);

        profileImageView = (CircleImageView) navigationView.findViewById(R.id.profile_image_view);
        usernameTextView = (TextView) navigationView.findViewById(R.id.username_text_view);
        usernameTextView.setText(username);
        Glide.with(this).load(R.drawable.profile).into(profileImageView);



    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_layout:
                // do your code

                setFragment(new HomeFragment());
                mTitle.setText("HOME");


                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                }

                break;

            case R.id.branches_layout:
                // do your code

                setFragment(new UpdateSecurityFragment());
                mTitle.setText("SELECT APPLICATIONS");


                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                }

                break;

            case R.id.alldeals_layout:
                // do your code
                setFragment(new UpdateLocationFragment());
                mTitle.setText("SELECT LOATION");

                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                }
//
//
                break;
            case R.id.signout_layout:
                // do your code

                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                }
                editor.putBoolean(Constants.E_BANKING_CHECKBOX, false);
                editor.putBoolean(Constants.M_BANKING_CHECKBOX, false);
                editor.putBoolean(Constants.ATM_CHECKBOX, false);
                editor.putString(Constants.FIRST_NAME, null);
                editor.putString(Constants.LAST_NAME, null);
                editor.putString(Constants.EMAIL, null);
                editor.putString(Constants.MOBILE_NUMBER, null);
                editor.putInt(Constants.SECURITY_STATUS, 0);
                editor.putInt(Constants.SECURITY_TYPE_ID, 0);
                editor.putInt(Constants.SELECTED_SECURITY_TYPE, 0);
                editor.commit();
                startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                finishAffinity();
//
//
                break;
            default:
                break;

        }
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_containerone, fragment);
        fragmentTransaction.commit();
    }



    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {


            isNetworkAvailable(context);

        }


    }
    private void showSnack(boolean isConnected) {

        if (!isConnected) {

            DrawerLayout Clayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            snackbar = Snackbar.make(Clayout, "Sorry! Not connected to internet", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    if (checkConnection()) {


                    } else {

                        showSnack(false);
                    }
                }
            });
            ;

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }

    }
    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        if (!isConnected) {
                            isConnected = true;
                            //do your processing here ---
                            //if you need to post any data to the server or get status
                            //update from the server
                            if (snackbar != null) {
                                snackbar.dismiss();

                            }







                        }
                        return true;
                    }
                }
            }
        }

        showSnack(false);
        isConnected = false;
        return false;
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
